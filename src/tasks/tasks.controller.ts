import { Controller, Get, Body, Post, Param, Delete, Patch, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { Task, TaskStatus } from './task.model';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTaskFilterDto } from './dto/get-task-filter.dto';

@Controller('tasks')
export class TasksController {

    constructor(private tasksService: TasksService){}

    @Get()
    getTasks(@Query() filterTaskDto:GetTaskFilterDto):Task[]{
        
        if(Object.keys(filterTaskDto).length){
            return this.tasksService.getTaskWithFilter(filterTaskDto);
        }else {
            return this.tasksService.getAllTasks();
        }
       
    }

    @Get('/:id')
    getTaskById(@Param('id') id:string):Task{
        return this.tasksService.getTaskById(id);
    }

    @Post()
    @UsePipes(ValidationPipe)
    createPost(@Body()createTaskDto:CreateTaskDto):Task{
      
        return this.tasksService.createTask(createTaskDto);
    }

    @Patch('/:id/status')
    updateTask(@Param('id')id:string,@Body('status') status:TaskStatus):Task{
        return this.tasksService.updateTask(id,status);
    }

    @Delete('/:id')
    deleteTask(@Param('id') id:string):void{
        this.tasksService.deleteTask(id);
    }
}
